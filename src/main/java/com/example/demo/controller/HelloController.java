package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/")
    public String index(){
        return "Spring is here!";
    }

    @GetMapping("/user/hello")
    public String helloUser(){
        return "bonjour simple utilisateur";
    }

    @GetMapping("/admin/hello")
    public String helloAdmin(){
        return "bonjour Monsieur l'administrateur !!!";
    }

}
